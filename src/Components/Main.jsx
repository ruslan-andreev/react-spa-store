import React, { useContext } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Context from './Context';
import DataNewGoods from '../DataNewGoods.json'
import Slider3 from './Slider3.jsx';


function Main (){
    const urlNewGoods = DataNewGoods     
    const [newProducts , setNewProducts] = useState(JSON.parse(localStorage.getItem('newProducts')) || [])
    const history = useHistory();
    const location = useLocation()
    const {setFilterStatus, setSearchValue} = useContext(Context);
    
    useEffect(()=>{
        if(location.pathname !== '/catalog'){
            setFilterStatus('default')
            setSearchValue('')
        }else return
    })

    useEffect(()=>{
        if(!localStorage.getItem('newProducts')){
            localStorage.setItem('newProducts',JSON.stringify(urlNewGoods))
            setNewProducts(urlNewGoods)  
        }
    },[urlNewGoods])

    function randomPicture(){ 
        return Math.floor(Math.random() * 3) + 1
    }
    
    function toCatalog (){
        history.push("/catalog")
    }

    function mainBtnFilter (value){
        history.push("/catalog")
        setFilterStatus(value)
    }

    function oldCollection (value){
        history.push("/catalog")
        setFilterStatus(value)
    }

    return(
        <>
            <section className="main__page">
                <div className="container__content">
                    <div className="wrapper__maine__title">
                        <div className="maine__title">
                            <h1>PORTEN</h1>
                            <p className="maine__title__sub">минск</p>
                        </div>
                        <p className="main__page__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Purus interdum purus, est tortor pulvinar ut in. Fringilla a diam enim sed justo, sed iaculis sagittis. Tortor id eu interdum nec ut iaculis. Penatibus ullamcorper ultricies morbi ipsum sem metus pharetra, mi. Tortor nibh magna feugiat id nunc, dui nisl viverra.</p>
                    </div>
                </div>
            </section>
            <section className="section__new-collection">
                <div className="new-collection">
                    <h2 className="section__title">новая коллекция</h2>
                    <div className="line"></div>
                    <button className="collection__btn" onClick={()=>toCatalog()}>каталог</button>
                </div>
                <div className="new-collection__items"><img src={`./image/products/watch${randomPicture()}.png`} alt="new-collection__img" /></div>
            </section>
            <section className="section__prev-collection">
                <div className="prev-collection"></div>
                <div className="prev-collection__description">
                    <h2 className="section__title">коллекция 2019-2020</h2>
                    <div className="line"></div>
                    <p className="page__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non rutrum ornare ut mattis habitant dui arcu. Sagittis amet nunc ut neque quis nibh arcu. Vivamus vestibulum nisi et venenatis sed scelerisque magna consectetur. Amet convallis quis gravida facilisis vulputate. Faucibus facilisi habitasse ipsum interdum dictum aliquet. Velit quis ullamcorper pulvinar nulla malesuada integer. Aenean praesent viverra nulla nullam natoque volutpat curabitur auctor. Viverra viverra ullamcorper scelerisque risus dignissim egestas. Id aliquam a aliquam egestas leo orci pharetra sed diam. </p>
                    <button className="collection__btn" onClick={()=>oldCollection('old')}>посмотреть коллекцию</button>
                </div>
            </section>
            <section className="section__newgoods">
                <h2 className="section__title">новые поступления</h2>
                <div className="line"></div>
                <div className="container__content">
                    <Slider3 
                        newProducts = {newProducts}
                    />
                </div> 
            </section>
            <section className="section__brands">
                <div className="container__content">
                    <h2 className="section__title">наши бренды</h2>
                    <div className="line"></div>
                    <div className="wrapper__brands">
                        <div className="brands__item"><img src="./image/brand.png" alt="brand"/></div>
                        <div className="brands__item"><img src="./image/brand.png" alt="brand"/></div>
                        <div className="brands__item"><img src="./image/brand.png" alt="brand"/></div>
                        <div className="brands__item"><img src="./image/brand.png" alt="brand"/></div>
                    </div>
                </div>
            </section>
            <section className="section__about">
                <div className="container__content">
                    <div className="about__description">
                        <h3>О магазине</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi semper viverra nunc cursus tortor lectus nunc nulla nibh. Egestas amet consectetur vel vitae aliquam dictum suspendisse. Lobortis eget consequat, tellus et et sed turpis. Pretium quisque vitae, amet, porttitor odio ultricies massa pharetra leo. Et ipsum urna fames in sit mi ultrices nisi, nunc.</p>
                    </div>
                    <div className="about__btn">
                        <h3>Категории</h3>
                        <button onClick={()=>mainBtnFilter('одежда')}>Одежда</button>
                        <button onClick={()=>mainBtnFilter('обувь')}>Обувь</button>
                        <button onClick={()=>mainBtnFilter('аксессуары')}>Аксессуары</button>
                    </div>
                    <div className="about__form">
                        <h3>Рассылка</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi semper viverra nunc cursus tortor lectus nunc nulla nibh.</p>
                        <form>
                            <input className="input__form" placeholder="E-mail" />
                            <button className="form__btn">подписаться</button>
                        </form>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Main;