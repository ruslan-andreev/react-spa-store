import React, {useState,useEffect, useContext} from "react";
import dataJson from '../Data.json'
import Product from "./Product";
import Filter from "./Filter";
import Context from "./Context"

function Catalog (){
    const data = dataJson
    const [products, setProducts] = useState(JSON.parse(localStorage.getItem('products')) || [])
    const {cart, setCart, searchValue, filter, setFilter, filterStatus} = useContext(Context)

    useEffect(()=>{
        if(!localStorage.getItem('products')){
            localStorage.setItem('products',JSON.stringify(data))
            setProducts(data)  
        }        
    },[data])
    
    useEffect(()=>{
        const productsFilter = JSON.parse(localStorage.getItem('products'))
        if(filterStatus === 'обувь'){
            setProducts([...productsFilter.filter((item)=>{
                return item.category === 'обувь'
            })])
        }else if(filterStatus === 'аксессуары'){
            setProducts([...productsFilter.filter((item)=>{
                return item.category === 'аксессуары'
            })])       
        }else if(filterStatus === 'одежда'){
            setProducts([...productsFilter.filter((item)=>{
                return item.category === 'одежда' 
            })])
        }else if(filterStatus === 'new'){
            setProducts([...productsFilter.filter((item)=>{
                return item.collection === 'new' 
              })])
        }else if(filterStatus === 'old'){
            setProducts([...productsFilter.filter((item)=>{
                return item.collection === 'old' 
              })])
        }else{
            setProducts([...productsFilter])
        }         
    },[filterStatus])

    function addToCart (product){
        cart.push(product)
        setCart([...cart])
        localStorage.setItem('cart', JSON.stringify(cart))
    }

    function filterClick(){
        setFilter(true)
    }

    return(
        <section className="section__catalog">
            <div className="container__content">
                <h2 className="section__title">каталог товаров</h2>
                <div className="line"></div>
                <div className="catalog__content">
                <div className="ctalog__filter__section">
                    <button className="ctalog__filter__btn" onClick={()=>filterClick()}><img src="./image/icons/filter_png.png" alt="filter" /></button>
                    {filter ? <Filter />: ''}
                </div>
                    <ul className="catalog__list">
                        {products.filter((item)=>{
                            if(searchValue.length === 0){
                                return item
                        }else if(item.title.toLowerCase().includes(searchValue.toLowerCase())) {
                            return item
                        }
                        return false;
                        }).map((product)=>{
                            return <Product 
                                    product={product}
                                    addToCart={addToCart}
                                    key={product.id}
                            />  
                        })}
                    </ul>  
                </div>
            </div>
        </section>
    )
}  
export default Catalog
