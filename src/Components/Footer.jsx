import React from 'react'

function Footer (){

    return(
        <footer className="footer">
            <div className="container__content">
            <div className="copyright">
                <span className="copyright__link">© Ruslan Andreev 2021</span>
                <span className="copyright__link">Сделано Figma.info</span>
            </div>
            </div>
        </footer>
    )
}
export default Footer;