import React from "react";
import Slider from "react-slick";


function Slider3 ({newProducts}){

    function sliderItem(){  
        return newProducts.map((product)=>{
            return  <div key={product.id} className="slider-item__container">
                        <div ><img src={product.picture} alt="slide"/></div>
                    </div>
        }) 
    }

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
      };

    return(
        <Slider {...settings}>
            {sliderItem()}
        </Slider>      
    )
}
export default Slider3;