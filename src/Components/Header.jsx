import React from 'react';
import Nav from './Nav';
import { NavLink } from 'react-router-dom'
import Search from './Search';
import Login from './Login';
import { useContext } from 'react';
import { useLocation } from 'react-router-dom';
import Context from './Context';

function Header (){
    const {cart, login, setLogin, navBar, setNavBar} = useContext(Context)
    const location = useLocation()

    function Count(){
        let count = 0
        if(cart.length === 0){
            return 
        }else{
            count = cart.length
        }
        return count
    }

    function loginClick(){
        setLogin(true)
    }
    function navBarClick(){
        setNavBar(!navBar)
    }
    
    return(
        <header className="header"> 
            <div className="header__section-contacts">
                <div className="container__content">
                    <div className="work__day">
                        <span className="phone__wrapper"><a className="tel" href="tel:8 (888) 888-88-88">8 (812) 123-45-67</a></span>
                        <span className='work__time'> Работаем 7 дней в неделю</span>
                        <span className='work__time'>9:00 — 18:00</span>
                    </div>
                    <div className="login">
                        <span className="login__link" onClick={()=>loginClick()}>Войти / Регистрация</span>
                        {login ? <Login /> : ''}
                    </div>
                </div>
            </div> 
            <div className="header__section-nav">
                <div className="container__content">   
                    <div className="logo">
                        <div className={navBar ? 'burger__btn active': 'burger__btn'} onClick={()=>navBarClick()}>
                            <span></span>
                        </div>
                        {navBar ? '': <NavLink to="/"><span className="logo__link">PORTEN</span></NavLink>}    
                    </div>
                    <div className="headerNav__wrapper">
                        <Nav />
                        <NavLink to="/cart">
                            <div className="header__cart"><img src="./image/icons/cart-icon.png" alt="cart-icon" /><div className="cartCounter">{Count()}</div></div>
                        </NavLink>
                        {location.pathname === "/catalog" ? <Search /> : ""}   
                    </div>
                </div>    
            </div>    
        </header>   
    )
}
export default Header;