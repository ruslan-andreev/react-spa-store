import React from "react";
import { useHistory } from 'react-router-dom'
import { useContext } from "react";
import Context from "./Context";


function ProductCard ({location}){
    const { title, description, price, picture } = location.state.product
    const {cart, setCart} = useContext(Context)
    const history = useHistory()
    
    function addToCart (){    
        cart.push(location.state.product)
        setCart([...cart]) 
        localStorage.setItem('cart', JSON.stringify(cart))
    }

    return(
        <section className="section__productCard">
            <div className="container__content">
                <div className="productCard">
                    <div className="productCard__img">
                        <img src={picture} alt={title} />
                    </div>
                    <div className="productCard__caption">
                        <p className="productCard__title">{title}</p>
                        <p className="productCard__description">{description}</p>
                        <div className="productCard__price__wrapper">
                            <span className="productCard__price">{price} BYN </span>   
                        </div>
                        <div className="productCard__btn">
                            <button className="productCard__addBtn" onClick={()=>addToCart(location)}>добавить в корзину</button>
                            <button className="back__btn" onClick={()=>history.goBack()}>назад</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default ProductCard;