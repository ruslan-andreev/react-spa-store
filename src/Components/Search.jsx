import React from 'react'
import { useContext } from 'react'
import Context from './Context'

function Search (){
    const {setSearchValue, setFilterStatus} = useContext(Context)

    function getInputValue (e) {
        setSearchValue(e.target.value)
        setFilterStatus('default')
    }
    return(
        <input type='text' id='header__widget__value' placeholder='Search' onChange={(event)=>{getInputValue(event)}} />
    )
}
export default Search