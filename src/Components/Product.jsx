import React from 'react'
import { useHistory } from 'react-router-dom'

function Product ({product, addToCart}){
    
    const history = useHistory()

    function heandlerProductView(){
        history.push(`product/${product.id}`,{product})
    }
    
    return(
        <li key={product.id} className="product__item">
            <div className="product__picture"><img src={product.picture} alt={product.title} /></div>
            <div className="product__caption">
                <p className="product__title" onClick={()=>{heandlerProductView()}}>{product.title}</p>
                <p className="product__description">{product.description}</p>
                <div className="price__wrapper">
                    <span className="product__price">{product.price}<br/>BYN </span>
                    <button className="product__addBtn" onClick={()=>{addToCart(product)}}>Добавить в корзину</button>
                </div>
            </div>
        </li>
    )
}
export default Product;