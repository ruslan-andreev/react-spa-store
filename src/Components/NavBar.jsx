import React from 'react'
import { NavLink} from 'react-router-dom'
import { useContext } from 'react'
import Context from './Context'

 function NavBar (){
    const {navBar, setNavBar} = useContext(Context)
    const active = navBar

    function closeNavBar(){
        setNavBar(false)
    }

    return(
        <div className={active ? 'nav-bar active' : 'nav-bar'} onClick={()=>closeNavBar()}>
            <div className="blur">
                <div className="nav__content" onClick={(e)=>e.stopPropagation()}>
                    <span className="logo__link">PORTEN</span>
                    <ul className="nav-bar__link">
                    <NavLink to="/">
                        главная
                    </NavLink>
                    <NavLink to="/catalog">
                        каталог
                    </NavLink>
                    <NavLink to="/contacts">
                        контакты
                    </NavLink>
                    </ul>
                </div>
            </div>
        </div>
     )
 }
 export default NavBar