import React, { useContext } from 'react'
import Context from './Context'

function Login (){
    const {setLogin} = useContext(Context)

    function loginClose (){
        setLogin(false)
    }

    return(
        <div className="modal__login" onClick={()=>loginClose()}>
            <div className="login__container" onClick={(e)=>e.stopPropagation()}>
                <p className="login__title">войти в личный кабинет</p>
                <form className="login__form">
                    <input type="email" placeholder="e-mail"/>
                    <input type="password" placeholder="password"/>
                    <div className="login-btn__wrapper">
                        <button className="login-btn__reg" onClick={(e)=>e.preventDefault()}>регистрация</button>
                        <button className="login-btn__in" onClick={(e)=>e.preventDefault()}>войти</button>
                    </div>
                </form>
                <button className="login__close-btn" onClick={()=>loginClose()}><img src="./image/icons/login-close.png" alt="close"/></button>
            </div>
        </div>
    )
}
export default Login