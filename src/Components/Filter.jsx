import React,{useEffect} from "react";
import { useContext } from "react";
import Context from "./Context"

function Filter (){
    const {setFilter, filterStatus, setFilterStatus, setSearchValue} = useContext(Context)

    useEffect(()=>{
        setSearchValue('')
    })
    function closeFilter(){
        setFilter(false)   
    }

    function getFilterStatus (value){
        setFilterStatus(value)
    }
   
    return(
        <div className="filter__modal" onClick={()=>closeFilter()}>
            <div className="filter__body" onClick={(e)=>e.stopPropagation()}>
                <p className="filter__title">выбрать категорию товара</p>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'default' ? true : false} onClick={()=>{getFilterStatus('default')}}/>Все товары</label>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'одежда' ? true : false} onClick={()=>{getFilterStatus('одежда')}}/>Одежда</label>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'обувь' ? true : false} onClick={()=>{getFilterStatus('обувь')}}/>Обувь</label>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'аксессуары' ? true : false} onClick={()=>{getFilterStatus('аксессуары')}}/>Аксессуары</label>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'new' ? true : false} onClick={()=>{getFilterStatus('new')}}/>Новая коллекция</label>
                <label><input type="radio" name="filter-name" defaultChecked={filterStatus === 'old' ? true : false} onClick={()=>{getFilterStatus('old')}}/>Прошлая коллекция</label>
                <button className="filter__close__btn" onClick={()=>closeFilter()}>закрыть</button>
            </div>
        </div>
    )
}
export default Filter