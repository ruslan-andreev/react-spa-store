import React from 'react'
import { NavLink } from 'react-router-dom'

function Nav (){

    return(
        <div className="wrapper__nav">
            <ul className="nav">
                <NavLink to="/">
                    главная
                </NavLink>
                <NavLink to="/catalog">
                    каталог
                </NavLink>
                <NavLink to="/contacts">
                    контакты
                </NavLink>
            </ul>
        </div>
    )
}
export default Nav;