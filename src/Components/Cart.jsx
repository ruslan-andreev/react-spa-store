import React from "react";
import Context from "./Context";
import { useHistory } from "react-router-dom";
import { useContext } from 'react';


function Cart (){
    const {cart, setCart} = useContext(Context)
    const history = useHistory()

    function backToCatalog(){
        history.push('/catalog')
    }

    function deleteCartItem (id){
       localStorage.setItem('cart', JSON.stringify(cart.filter(item=>{
            return item.id !== id
       })))
       setCart(JSON.parse(localStorage.getItem('cart')))
    }

    function clearCart(){
        cart.length = 0
        setCart([...cart])
        localStorage.setItem('cart', JSON.stringify(cart))
    }

    function totalCost(){
        let sum = 0
        if(cart.length === 0){
            return
        }else{
            cart.forEach((product)=>{
                sum += parseInt(product.price)
            })   
        }
        return sum.toLocaleString()
    }

    return(
        <section className="section__cart">
            <div className="container__content">
                <h2 className="section__title">корзина покупок</h2>
                <div className="line"></div>
                <div className="cart__content">     
                    {!localStorage.getItem('cart') || JSON.parse(localStorage.getItem('cart')).length === 0 ?
                        <p className="cart__alert">в корзине нет выбранных товаров</p> :
                        <ul className="cart__list">
                            {cart.map((productCart, index)=>{
                                return(
                                    <li key={index} className="cart__item" >
                                        <div className="cartItem__picture">
                                            <img src={productCart.picture} alt={productCart.title} />
                                        </div>
                                        <div className="cart-description__wrapper">
                                            <div className="cartItem__description">
                                                <p className="productCart__item-title">{productCart.title}</p>
                                                <p className="productCart__item-price">{productCart.price} BYN</p>
                                            </div>
                                            <button className="deleteCart__item"  onClick={()=>deleteCartItem(productCart.id)}>убрать товар</button>
                                        </div>
                                    </li>    
                                )  
                            })}
                            <p className="total__cost">итого: {totalCost()} BYN</p>
                            <div className="cartBtn__wrapper">
                                <button className="backToCatalog__btn" onClick={()=>backToCatalog()}>перейти в каталог</button>
                                <button className="clearCart__btn" onClick={()=>clearCart()}>очистить список покупок</button>
                            </div>
                        </ul>
                    }  
                </div>
            </div>
        </section>
    )
}
export default Cart;

