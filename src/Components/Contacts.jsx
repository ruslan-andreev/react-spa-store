import React from "react";
import { useLocation } from "react-router";
import { useEffect } from "react";
import { useContext } from "react";
import Context from './Context';

function Contacts (){
    const {setFilterStatus, setSearchValue} = useContext(Context);
    const location = useLocation()
    
    useEffect(()=>{
        if(location.pathname !== '/catalog'){
            setFilterStatus('default')
            setSearchValue('')
        }else return
    })

    return(
        <section className="section__contacts">
            <div className="container__content">
                <h2 className="section__title">наши контакты</h2>
                <div className="line"></div>
                <div className="contacts__content">
                        <div className="contacts__item">
                            <h3 className="contact__title">Позвоните или напишите нам:</h3>
                            <p><a className="contacts__tel" href="tel:+375 17 221-01-48">+375 17 666-66-66</ a></p>
                            <p><a className="contacts__tel" href="tel:+375 29 800-45-33">+375 66 666-66-66</ a></p>
                            <p><a className="contacts__mail" href="https://info@porten.by">info@yyyyyy.by</a></p>
                        </div>
                        <div className="contacts__item">
                            <h3 className="contact__title">Юридический/почтовый адрес:</h3>
                            <p>220034, г.Минск, ул.Чапаева, д.3, пом.36, блок 11</p>
                        </div>
                        <div className="contacts__item">
                            <h3 className="contact__title">Адрес магазина:</h3>
                            <p>220088, г. Минск, ул. Пулихова, д. 23-2Н, цокольный этаж</p>
                        </div>
                        <div className="contacts__item">
                            <h3 className="contact__title">Банковские реквизиты:</h3>
                            <p>УНП 999999519,<br/>Р/С BY999999999999988883 ЗАО «БСБ Банк»,<br/>БИК UNBSBYXXXX</p>
                        </div>
                </div>                   
            </div>  
            <iframe title="unique title" className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2350.8900063663673!2d27.57830401530632!3d53.898158940936305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbcfc9a3c4b6cb%3A0xb59a20c1eab0268b!2z0YPQu9C40YbQsCDQn9GD0LvQuNGF0L7QstCwIDIzLCDQnNC40L3RgdC6!5e0!3m2!1sru!2sby!4v1632515815024!5m2!1sru!2sby" width="600" height="450" style={{border:0}} allowFullScreen="" loading="lazy"></iframe>
        </section>
    )
}
export default Contacts;