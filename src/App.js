import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import '../src/css/style.css'
import '../src/css/media.css'
import Header from './Components/Header';
import Main from './Components/Main';
import Contacts from './Components/Contacts';
import Catalog from './Components/Catalog';
import Footer from './Components/Footer';
import ProductCard from './Components/ProductCard';
import Cart from './Components/Cart';
import Context from './Components/Context';
import { useState } from 'react';
import NavBar from './Components/NavBar'


function App() {
  const [cart , setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
  const [searchValue, setSearchValue] = useState('')
  const [filter, setFilter] = useState(false)
  const [filterStatus, setFilterStatus] = useState('default')
  const [login, setLogin] = useState(false)
  const [navBar, setNavBar] = useState(false)
  
  return (
    <BrowserRouter>
    <Context.Provider value={{cart, setCart, searchValue, setSearchValue, filter, setFilter, filterStatus, setFilterStatus, login, setLogin, navBar, setNavBar}}>
      <Header />
      
      <Switch>
        <Route exact path="/">
          <Main />
        </Route>
        <Route path="/catalog">
          <Catalog />
        </Route>
        <Route path='/contacts'>
          <Contacts />
        </Route>
        <ProductCard path="/product/:id" />
        <Cart path='/cart'/>
      </Switch>
      <Footer />
      <NavBar />
    </Context.Provider>  
    </BrowserRouter>
  );
}

export default App;
